// klasa uzywana do budowania pojedynczego wyniku wyszukiwania
class BuilderWyniku {
	constructor() {
		// konstruujac obiekt budowniczego przypiszemu mu nowy element htmlowy, na którym będzie oparty (w index.html element o id szablonWyniku)
		// szablon ten klonujemy (kopiujemy) i usuwamy jemu atrybut id (to juz nie bedzie szablon wyniku, tylko konkretny wynik)
		this.$wynik = $("#szablonWyniku").clone();
		this.$wynik.removeAttr("id");
	}
	// metoda ustawiajaca tytul wynikowi wyszukiwania
	ustawTytul(tytul) {
		this.$wynik.find(".tytul").text(tytul);
	}
	// metoda ustawiajaca opis wynikowi wyszukiwania
	ustawOpis(opis) {
		this.$wynik.find(".opis").text(opis);
	}
	// metoda link obrazka
	ustawObrazek(linkObrazka) {
		this.$wynik.find("img").attr("src", linkObrazka).css("display", "block");
	}
	// metoda ustawia link wynikowi wyszukiwania
	ustawLink(link) {
		this.$wynik.find(".link").text(link);
		// gdy klikniemy wynik wyszukiwania otwarte zostanie nowe okno z odpowiednim adresem
		this.$wynik.click(function() {
			window.open(link);
		});
	}
	// metoda zwracajaca utworzony element htmlowy
	pobierzWynik() {
		return this.$wynik;
	}
}