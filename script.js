// funkcja wyszukuje wyniki wyszukiwania zgodnie z tekstem z pola tekstowego
function szukaj() {
	// ukrywamy okienko podpowiedzi autocomplete (jesli jest pokazane)
	$("#podpowiedzi").hide();
	// sczytujemy wartosc z pola tekstowego
	var tekstDoWyszukania = $("#tekstDoWyszukania").val();
	// wysylamy zapytanie get, które zwróci nam json z danymi dotyczacymi wynikow wyszukiwania (tytuly, adresy, opisy, miniaturki)
	$.getJSON("https://pl.wikipedia.org/w/api.php?action=query&inprop=url&format=json&gsrlimit=15&generator=search&prop=pageimages|extracts|info&pithumbsize=300&exintro&explaintext&exsentences=1&exlimit=max&origin=*&gsrsearch=" + tekstDoWyszukania, function(data){
		// jesli dostalismy poprawna odpowiedz
		if(data.query != null && data.query.pages != null) {
			// to sczytujemy dane dotyczace stron
			var pages = data.query.pages;
			// przypisujemy do zmiennej element ze strony o id wyniki i "czyscimy go" (jesli wczesniej pojawily sie wyniki wyszukiwania, to ich sie
			// pozbywamy)
			var $wyniki = $("#wyniki");
			$wyniki.empty();
			// dla kazdej ze znalezionych stron
			for(var pageId in pages) {
				var page = pages[pageId];
				// tworzymy builder wyniku korzystajac z klasy BuilderWyniku
				var builderWyniku = new BuilderWyniku();
				// ustawiamy dane
				builderWyniku.ustawTytul(page.title);
				builderWyniku.ustawOpis(page.extract);
				if(page.thumbnail != null && page.thumbnail.source != null) {
					builderWyniku.ustawObrazek(page.thumbnail.source);
				}
				builderWyniku.ustawLink(page.fullurl);
				// do listy wynikow dodajemy utworzony przez builder wynik
				$wyniki.append(builderWyniku.pobierzWynik());
			}
		}
	});
}

// funkcja konstruuje na podstawie przekazanych parametrow wiersz wyswietlany w pokazujacym sie okienku podpowiedzi (autocomplete)
function skonstruujWierszPodpowiedzi(slowo, rozwiniecie) {
	var $podpowiedz = $("<div class='podpowiedz'><span class='slowo'>" + slowo + "</span> - <span class='rozwiniecie'>" + rozwiniecie.substring(0, 10) + "(...)</span></div>");
	// gdy klikniemy w tworzony wiersz podpowiedzi, to wypelnia sie wartosc pola tekstowego i nastepuje wyszukanie
	$podpowiedz.click(function() {
		$("#tekstDoWyszukania").val(slowo);
		szukaj();
	});
	return $podpowiedz;
}

// funkcja wyszukuje podpowiedzi (autocomplete) dla pola tekstowego
function wyszukajPodpowiedzi(tekstDoWyszukania) {
	$.getJSON("https://pl.wikipedia.org/w/api.php?action=opensearch&origin=*&search=" + tekstDoWyszukania + "&namespace=0&format=json", function(data){
		$("#podpowiedzi").show();
		$("#podpowiedzi").empty();
		if(data[1].length > 0) {
			for(var i = 0; i < data[1].length; i++) {
				$("#podpowiedzi").append(skonstruujWierszPodpowiedzi(data[1][i], data[2][i]));
			}
		}
	});
}

// gdy zostanie wpisane cos nowego do pola tekstowego (gdy podniesiony zostanie klawisz klawiatury)
$("#tekstDoWyszukania").keyup(function() {
	var tekstDoWyszukania = $("#tekstDoWyszukania").val();
	// to jesli tekst w polu tekstowym ma co najmniej trzy znaki
	if(tekstDoWyszukania.length >= 3) {
		// wyszukujemy podpowiedzi
		wyszukajPodpowiedzi(tekstDoWyszukania);
	} else {
		// w innym wypadku ukrywamy podpowiedzi
		$("#podpowiedzi").hide();
	}
});

// gdy klikniemy przycisk Szukaj, wywolujemy funkcje szukaj
$("#przyciskSzukaj").click(szukaj);